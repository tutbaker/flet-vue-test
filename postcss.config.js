module.exports = {
  plugins: {
    autoprefixer: {},
    "postcss-import": {},
    "postcss-advanced-variables": {},
    "postcss-color-mod-function": {},
    "postcss-preset-env": { stage: 0 },
    "postcss-nested": {},
    "postcss-url": {},
    "postcss-inline-svg": {},
    "postcss-extend": {},
    "postcss-browser-reporter": {},
    "postcss-reporter": {
      clearReportedMessages: true,
    },
  },
};
